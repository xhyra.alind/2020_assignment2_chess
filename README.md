[Università degli Studi di Milano - Bicocca]{.smallcaps}\
**Scuola di Scienze**\
**Dipartimento di Informatica, Sistemistica e Comunicazione**\
**Corso di laurea in Informatica**\

**Processo di Elicitation per una applicazione di Scacchi**

**Gruppo:**\
Alind Xhyra, Matricola 829865 \
Davide Pietrasanta, Matricola 844824 \
**GitLab:** <https://gitlab.com/xhyra.alind/2020_assignment2_chess>

# Introduzione

In questo assignment ci occuperemo del processo di Elicitation per una
applicazione di Scacchi, occupandoci della ricerca ed intervista degli
stakeholder fino alla ricerca dei requisiti.

La scelta di questo progetto è nata dalla volontà di realizzare una
nostra applicazione per gli scacchi su cui potremo in futuro realizzare
un modello *Machine Learning* per partite *giocatore **vs** ia* e *ia
**vs** ia*.

## Group Session

Ci siamo riuniti per discutere sulla realizzazione del progetto e siamo
giunti all'ideazione di una semplice applicazione che permette di
giocare partite di scacchi su una scacchiera virtuale.

Buona parte del lavoro sarà concentrata sul *Background*, ovvero la
struttura e le regole del gioco.

Riguardo gli *stakeholder* abbiamo concordato che bastano:

-   Un giocatore esperto

-   Un giocatore intermedio

-   Un giocatore novizio

Abbiamo concorda che a ciascuno dei tre *stakeholder* verrà
somministrata un'intervista col fine di trovare i requisiti per ciascuna
categoria di utenza. Preferiamo un'intervista piuttosto che un
questionario per riuscire a capire più informazioni possibili, visti i
pochi *stakeholders*.

## Suddivisione Progetto

Questo progetto sarà suddiviso in capitoli:

-   Chapter [2](#chap:Scenari){reference-type="ref"
    reference="chap:Scenari"}: esporrà gli scenari da tenere
    maggiormente in considerazione.

-   Chapter [3](#chap:Stakeholders){reference-type="ref"
    reference="chap:Stakeholders"}: esporrà le varie interviste agli
    *stakeholder* e i requisiti individuati

-   Chapter [4](#chap:Workflow){reference-type="ref"
    reference="chap:Workflow"}: esporrà il *workflow* seguito per il
    progetto.

-   : discuterà del *backgound study*, il gioco degli scacchi e le sue
    regole.

# Scenari {#chap:Scenari}

Dopo il *Background Study*, presente in appendice
[5](#app:Background){reference-type="ref" reference="app:Background"},
ne risulta che gli scenari da tenere maggiormente in considerazione
sono:

-   Patta

-   Arrocco

-   En passant e Promozione pedone

-   Cavallo salta alleati

-   Scacco e Scacco Matto

## Patta

**Patta** è il termine con cui nel gioco degli scacchi si indica una
partita terminata in parità. È definita dall'art. 9 del regolamento
FIDE.\
Il gioco termina in patta (parità) nei seguenti casi:

-   Se restano sulla scacchiera soltanto i due re;

-   Se la situazione è tale per cui nessuno dei due giocatori può dare
    scacco matto all'altro, anche in caso di difesa peggiore.

-   Se si verifica uno **Stallo**, che avviene quando un giocatore non
    ha mosse legali disponibili pur non essendo sotto scacco.

La partita termina con una **patta su richiesta di un giocatore** se
questi effettua o ha intenzione di effettuare (avendola segnata sul
formulario e avendo espresso l'intenzione di eseguirla) una mossa che:

-   Sarà l'ultima di una serie di cinquanta mosse consecutive (cinquanta
    mosse per ciascun giocatore) nelle quali non è stato catturato alcun
    pezzo e non è stato mosso alcun pedone;

-   Comporta la ripetizione sulla scacchiera della stessa posizione per
    tre volte (anche non consecutive) durante la partita. La posizione è
    considerata identica se la mossa spetta al medesimo giocatore, se
    tutti i pezzi del medesimo genere e colore si trovano sulle stesse
    case e se sussistono inalterate le stesse possibilità di movimento
    (inclusi arrocco e catture en passant).

Nella pratica di torneo il giocatore si rivolgerà all'arbitro il quale,
dopo aver verificato che le condizioni necessarie siano soddisfatte, ha
l'obbligo di dichiarare patta la partita.\
Un giocatore che ha appena eseguito la propria mossa può, prima di
azionare l'orologio, proporre all'avversario un'offerta incondizionata
di patta, una **Patta d'accordo**. Tale offerta può essere accettata ed
in tal caso la partita termina immediatamente come patta. Se viene
rifiutata la partita continua normalmente. Di recente, in alcuni tornei
professionistici, è stata introdotta una regola per cui non è ammessa la
patta d'accordo (la cosiddetta regola di Sofia). Questo, secondo i
promotori di questa iniziativa, favorirebbe la combattività dei
giocatori e aumenterebbe l'interesse per il pubblico verso gli scacchi.\

## Arrocco

L'**Arrocco** è una mossa particolare nel gioco degli scacchi che
coinvolge il re e una delle due torri. È l'unica mossa che permette di
muovere due pezzi contemporaneamente nonché l'unica in cui il re si
muove di due caselle.\
Consiste nel muovere il re di due caselle a destra o a sinistra in
direzione di una delle due torri e successivamente muovere la torre
(quella verso la quale il re si è mosso) nella casa compresa tra quelle
di partenza e di arrivo del re.\
Per indicare l'intenzione di effettuare un arrocco si deve prima
sollevare il re e muoverlo di due case e solo successivamente muovere la
torre nella casa di destinazione. Se si tocca la torre per prima si deve
effettuare, secondo la regola che il pezzo toccato dev'essere mosso, una
mossa con la sola torre. Se si arrocca muovendo o toccando
contemporaneamente il re e la torre, si commette una mossa illegale.\
La mossa può essere effettuata solo se si è in presenza delle seguenti
condizioni:

-   Il giocatore non ha mai mosso il re;

-   Il giocatore non ha mai mosso la torre coinvolta nell'arrocco;

-   Non ci sono pezzi tra il re e la torre coinvolta;

-   Il re e la torre devono trovarsi sulla stessa traversa (questa
    regola venne introdotta nel 1972 quando si scoprì che le regole
    altrimenti permettevano l'arrocco in verticale con un pedone
    promosso a torre; vedi Arrocco Pam-Krabbé);

-   Il re non deve essere sotto scacco;

-   Il re, durante il movimento dell'arrocco, non deve attraversare
    caselle in cui si troverebbe sotto scacco.

L'arrocco non è vietato se ad essere sotto attacco (prima, durante o al
termine della mossa) è la torre.\
Il Bianco può compiere un arrocco lungo, mentre il Nero può compiere un
arrocco corto.\

    

L' **Arrocco Pam-Krabbé** è una ipotetica e bizzarra mossa degli scacchi
che consiste nell'arroccare in verticale sulla colonna e con una torre
ottenuta per promozione.Le regole FIDE vennero tuttavia modificate nel
1972 aggiungendo la condizione che re e torre coinvolti nell'arrocco
debbano essere sulla medesima traversa. La nuova formulazione del
regolamento ha escluso quindi la validità dell'arrocco Pam-Krabbé.\

## En passant e Promozione del pedone

Il **Pedone** è un pezzo su cui bisogna porre particolari attenzioni.
Esso viene coinvolto in due eventi, cioè **en passant** e la
**Promozione**.\
Nel gioco degli scacchi la presa **en passant** o **presa al varco** è
una mossa speciale, come l'arrocco, cioè un'eccezione alle normali
regole del movimento dei pezzi.\
Quando un pedone, muovendosi di due passi (quindi per la prima volta),
finisce esattamente accanto (sulla stessa traversa e su colonna
adiacente) ad un pedone avversario, nella mossa successiva quest'ultimo
può catturarlo come se si fosse mosso di un passo solo.\
È importante notare che, quando un pedone ha la possibilità di
effettuare una presa en passant nei confronti di un pedone avversario,
deve realizzarla subito, al verificarsi della posizione, altrimenti
perde il diritto a farlo.\
Nella notazione algebrica la presa *en passant* viene indicata con
l'abbreviazione e.p..\
La **Promozione**, nel gioco degli scacchi, è il raggiungimento da parte
di un qualsiasi pedone bianco dell'ottava traversa o di un pedone nero
della prima, che permette al pedone stesso di \"essere promosso\" al
rango di pezzo dello stesso colore. Tale pezzo viene scelto dal
giocatore tra donna, torre, alfiere e cavallo indipendentemente da quali
e quanti pezzi siano già presenti sulla scacchiera. Si noti comunque che
un pedone non può essere promosso a re. È possibile attuare il
cambiamento del pedone soltanto dopo un turno successivo all'arrivo del
fondo.\

## Cavallo

Nel gioco degli scacchi il Cavallo è uno dei pezzi a disposizione dei
giocatori ed è l'unico a cui è permesso \"saltare\" i pezzi, sia
alleati, sia avversari e quindi l'unico dietro i pedoni che all'inizio
può essere mosso senza bisogno di spostare prima quest'ultimi.\

## Scacco e Scacco Matto

**Scacco al re**, o più semplicemente **scacco**, indica il fatto che il
re sia sotto minaccia di presa da parte di un pezzo avversario. Alla
mossa seguente il giocatore il cui re è sotto scacco deve rimuovere la
condizione: se nessuna mossa consentisse di fare ciò, lo scacco è detto
matto (**Scacco Matto**) e la partita termina con la vittoria
dell'avversario.\
Nel corso della partita una mossa è considerata illegale se lascia il
proprio re sotto scacco. Si noti anche che non si può dare scacco al re
avversario con il proprio re in quanto questo verrebbe a trovarsi a sua
volta sotto scacco.\
Dare scacco al re avversario in una partita di scacchi non assegna punti
di vantaggio per vincere e talvolta non è nemmeno utile. Uno scacco
inutile può persino dare al giocatore che lo subisce un tempo di
vantaggio per poter muovere il proprio re in una posizione più
vantaggiosa. In ogni caso esistono molti casi in cui dare scacco al re
avversario può essere un tattica utile (o parte di essa), sia in una
situazione di attacco che in una di difesa.

# Stakeholder {#chap:Stakeholders}

## Elenco stakeholder

-   Un giocatore esperto

-   Un giocatore intermedio

-   Un giocatore inesperto

-   Team di sviluppo

Il **punteggio ELO** indica la forza di un giocatore in base ai
risultati che ha ottenuto nelle competizioni scacchistiche. Ogni
Federazione nazionale ha una sua graduatoria ELO, non necessariamente
coincidente con quelle delle altre federazioni. Tuttavia la graduatoria
più importante in assoluto è quella che viene emanata dalla FIDE ogni
anno. Il sistema ELO tiene conto della differenza di valore dei
giocatori. Per esempio, nel caso di una partita patta un giocatore di
medio livello guadagna parecchi punti se ha giocato contro un Maestro.\
Inoltre nel sistema ELO ogni categoria cade in un determinato intervallo
di punti:

-   2700 ed oltre = Informalmente *Super Grandmasters*

-   2500/2700 = Gran Maestro (GM)

-   2400/2500 = Maestro Internazionale (IM)

-   2300/2400 = FIDE Maestro (FM)

-   2200/2300 = FIDE Candidato Maestro (CM)

-   2000/2200 = Candidato Maestro (USA)

-   1800/2000 = Classe A, categoria 1

-   1600/1800 = Classe B, categoria 2

-   1400/1600 = Classe C, categoria 3

-   1200/1400 = Classe D, categoria 4

-   sotto i 1200 = Novizio

Noi andremo a considerare dei margini più flessibili e informali di
quelli definiti dalla FIDE per gli *stakeholders*. Consideriamo:

-   Un giocatore esperto = 1600 e oltre

-   Un giocatore intermedio = 1200/1600

-   Un giocatore inesperto = Sotto i 1200 o non registrato

## Motivazioni

Pensiamo che considerare giocatori con diversi livelli di esperienza sia
la migliore scelta per creare un prodotto adatto a tutti i tipi di
giocatori. Pensiamo inoltre che i tipi di *stakeholders* richiesti sia
ragionevole, abbiamo per questo escluso Gran Maestri o giocatori con ELO
troppo altro perché non rintracciabili con i nostri fondi.\
Vogliamo prendere in considerazione anche i giocatori inesperti, o
saltuari, per produrre un'applicazione con un migliore usabilità.\

## Grafico Interesse/Potere

\|c\|c\|c\|c\| &\
& Alto & Basso\
& Alto & *Giocatore Intermedio* & *Team di sviluppo*\
& Basso & *Giocatore Esperto* & *Giocatore Inesperto*\

## Intervista

Prime di intervistare lo *stakeholder* andremo ad illustrargli
brevemente lo scopo della nostra applicazione scacchistica. In seguito
procederemo con l'intervista.

Le domande per l'intervista sono:

1. Come ti chiami? Età? Da quanto tempo giochi? Hai mai partecipato a tornei? Se sì qual è il tuo ELO?

2. Hai mai giocato a scacchi su applicazioni? Se sì cosa ti è piaciuto? Cosa non ti è piaciuto? Cosa ti piacerebbe trovare di nuovo? Cosa cambieresti di quelle che hai utilizzato?

3. Abbiamo notato diversi tipi di partite con diversi tempi possibili, quale consigli e su quale criterio?

4. Quale stile di pezzi consigli? Quale colore e dimensione trova sia la migliore per giocare? Inoltre sapresti illustrarmi cosa sono i pezzi *Staunton*?

5. Preferiresti poter scegliere tra stili diversi di scacchiera e di pezzi?

6. Ritieni necessario un sistema di *ranking*? Ritieni che il sistema di *ranking* della FIDE possa andar bene per un'applicazione? Consigli qualche altro sistema di *ranking*?

7. Ritieni utile offrire la possibilità a fine partita di salvare lo storico delle mosse effettuate? Ritieni utile anche una funzione di *replay* delle partite a partire dallo storico delle mosse?

8. Qauli criteri dobbiamo considerare per l'algoritmo di *matchmaking*? Prefersci un abbinamento rapido ma potenzialmente sbilanciato o uno un po' più lento ma preciso ed efficace?

9. So che in molti tornei ultimamente non è più possibile una patta su richiesta di un giocatore, per incentivare la combattività, ritieni che questa restrizione possa andar bene per la nostra applicazione?

10. Quali altre funzioni rintieni necessarie e vorresti che fossero introdotte nella nostra applicazione?

Scopo delle domande:

1.  Identificare lo *stakeholder*.

2.  Capire cose piace e cosa no nelle applicazioni per scacchi già
    presenti sul mercato e cosa vorrebbero cambiare.

3.  Capire se bisogna implementare un solo tipo di partita con tempi
    prestabiliti o lasciare maggiore scelta all'utente con la
    possibilità di creare partite *custom* con tempi personalizzati.

4.  Studio sull'Interazione Uomo Macchina per portare l'applicazione ad
    avere una migliore interfaccia con l'utente ed essere più gradevole
    ed immediata possibile.

5.  Ci sono molti sistemi di *ranking* che vengono applicati al
    *system-as-is* ma non è detto che questi sistemi siano utili per la
    nostra applicazione, scopo dell'intervista è trovare i sistemi di
    *ranking* più adatti per la nostra applicazione.

6.  Capire su che dati costruire il sistema di *matchmaking* per le
    partite e quanto rapido deve essere.

7.  Capire se è necessario introdurre un sistema di salvataggio dello
    storico delle mosse di una partita terminata e della funzione di
    *replay* delle partite svolte.

8.  Il nostro team pensa che la patta debba esser aggiunta per rendere
    l'applicazione più accessibile ma vorrebbe prima sentire le opinioni
    degli *stakeholder*.

9.  Trovare eventuali funzionalità extra inizialmente non valutate
    proposte dagli *stakeholder*.

# Workflow {#chap:Workflow}

![image](images/workflow.png){height="0.65\\textheight"}

Per semplicità e compattezza si rinominerà il gruppo di stakeholder
Giocatore Inesperto, Giocatore Intermedio, Giocatore esperto in
**Giocatori**.

## Background study

**Stakeholder coinvolti:** Team di sviluppo.

**Obiettivi:** Studiare il *system-as-is*.

Il background study è stato ampiamente esposto nell'

## Brainstorming

**Stakeholder coinvolti:** Team di sviluppo.

**Obiettivi:** Discutere dei *system-as-is* individuati, stilare una
lista iniziale di requisiti e gli scenari individuati.

**Requisiti funzionali individuati:** Interazione dei giocatori con la
scacchiera, gestione dello storico partite, gestione del ranking,
sistema di matchmaking, gestione delle mosse, gestione del tempo per
mossa.

**Requisisti non funzionali individuati:** Velocità di caricamento,
interfaccia grafica *user friendly*, persistenza locale e su db remoto
dello storico partite e ranking, mantenibilità del sistema, sicurezza
dei dati personali degli utenti (email, nome).

Gli scenari più critici individuati sono stati esposti nel Chapter
[2](#chap:Scenari){reference-type="ref" reference="chap:Scenari"}

## Intervista

**Stakeholder coinvolti:** Team di sviluppo, Giocatori.

**Obiettivi:** Discutere l'idea, i requisiti individuati e trovarne di
nuovi.

L'intervista coinvolge giocatori di livello diverso, con lo scopo di
chiarire dubbi sui *system-as-is*, individuati e di valutare proposte e
requisiti non presi in considerazione durante la fase di
*brainstorming*.

L'intervista, insieme alle categorie di utenza coinvolte e agli
obiettivi sono state esposti nel Chapter
[3](#chap:Stakeholders){reference-type="ref"
reference="chap:Stakeholders"}.

## Sessione di gruppo\*

**Stakeholder coinvolti:** Team di sviluppo, Giocatori.

**Obiettivi:** Validazione e discussione dei requisiti ottenuti.

Nella fase di sessione di gruppo tutti gli *stakeholder* coinvolti si
riuniscono con l'obiettivo di valutare i risultati delle interviste e
validare i requisiti ottenuti e gli scenari individuati, andando
eventualmente ad integrarne altri.

## Storyboard e scenari\*

**Stakeholder coinvolti:** Team di sviluppo.

**Obiettivi:** Integrazione delle funzionalità e dei requisiti,
definizione interfaccia utente

In questa fase vengono integrati i requisiti, definiti tutti gli scenari
e realizzato il primo prototipo dell'applicazione andando ad integrare
tutte le funzionalità previste.

## Test Prototipo\*

**Stakeholder coinvolti:** Team di sviluppo, Giocatori.

**Obiettivi:** Validazione del prototipo.

Il prototipo realizzato viene sottoposto a test da parte degli
*stakeholder* in tutte le funzionalità e i diversi scenari, con
l'obiettivo di trovare elementi da migliorare e per poterlo validare.

[^1]

# Background Study {#app:Background}

Prima di interagire con gli *stakeholders* è meglio eseguire un
*background study* per studiare al meglio il *system-as-is* così da
porre domande mirate.\
Le regole ufficiali degli scacchi reperibili nel Manuale FIDE,
facilmente reperibile.\

## La scacchiera

Gli scacchi si giocano su una tavola quadrata, detta scacchiera, divisa
in 64 case, alternativamente di colore chiaro e scuro, organizzate in 8
righe orizzontali (o \"traverse\") e verticali (o \"colonne\"): le
traverse sono numerate da 1 a 8 (rispettivamente, base dei pezzi bianchi
e dei neri), mentre le colonne sono contrassegnate dalle lettere
dell'alfabeto da \"a\" a \"h\". La scacchiera deve essere orientata in
modo che la casa nell'angolo in basso a destra di ciascun giocatore sia
chiara.\
Le case della scacchiera nelle competizioni ufficiali devono avere lato
compreso tra i 54,5 e i 56 mm. I tavoli da scacchi devono avere una
lunghezza minima doppia rispetto a quella della scacchiera, mentre la
larghezza deve superare quella della scacchiera di 15-20 cm.\
Sulla scacchiera si muovono 32 pezzi, di cui 16 bianchi e 16 neri. Le
scacchiere e i pezzi impiegati nei tornei ufficiali possono essere in
legno, plastica o materiali con caratteristiche simili. Esistono anche
scacchiere artistiche con pezzi in vetro, pietra, cuoio o metallo,
usate, più che altro, come oggetti decorativi.\

## I pezzi sulla scacchiera

Ogni giocatore dispone di un insieme di 16 pezzi, ciascuno composto di
sei tipi diversi di pezzi. I due insiemi di pezzi sono colorati in modo
differente: in genere uno è molto più chiaro dell'altro, e prendono
dunque il nome di Bianchi e Neri. In condizioni ufficiali il colore dei
bianchi deve essere bianco, crema o una tonalità di colore intermedia
fra esse, mentre i neri devono essere neri, marroni o di tonalità
intermedia. Tale colorazione può anche essere fornita dal colore
naturale del legno, se sono realizzati con tale materiale. I pezzi non
devono essere di colore lucido.\
L'aspetto dei pezzi nei tornei ufficiali deve essere di tipo *Staunton*.
I pezzi devono essere chiaramente distinguibili fra loro, in particolare
la sommità del re deve essere differente da quella della donna:
comunemente sul re è collocata una croce e sulla donna una corona.
L'alfiere può recare una tacca sulla sua sommità o questa può essere di
un differente colore. Il re di un set regolamentare da torneo deve avere
un'altezza compresa tra 84 e 100 mm e una base che sia il 40-50 percento
della sua altezza. Gli altri pezzi devono essere proporzionati al re e
il peso dei pezzi deve garantirne la stabilità, consentendo comunque di
muoverli agevolmente. Per assicurare ciò, i pezzi sono in genere
piombati, ovvero contengono all'interno dei pesi metallici (tipicamente
in piombo) che ne assicurano la stabilità.\

La **donna** e la **torre** sono detti «pezzi pesanti», in quanto sono
in grado da soli di dare matto con l'aiuto del solo re; invece
l'**alfiere** e il **cavallo** sono detti «pezzi leggeri», in quanto non
sono in grado farlo.\
I **pedoni** occupano rispettivamente la seconda e la settima traversa,
mentre gli altri pezzi prendono posizione nella prima e nell'ottava
traversa. A partire dai due angoli, in modo simmetrico, ogni giocatore
posiziona torre, cavallo e alfiere e, per concludere, la donna, sulla
casa del proprio colore rimasta libera, e il re, nella casa di colore
opposto.\
Il giocatore che muove per primo è quello che vede il proprio re a
destra della propria donna, che si chiama convenzionalmente «Bianco»,
mentre l'altro è detto «Nero».\

## Movimento dei pezzi

Ciascun pezzo degli scacchi si muove con precise modalità.Nessun pezzo
può andare a occupare una casa in cui è presente un altro pezzo dello
stesso schieramento; può invece muoversi su una casa occupata da un
pezzo avversario, effettuando in tal caso una \"cattura\", cioè
eliminando (\"mangiando\") dalla scacchiera il pezzo nemico e prendendo
il suo posto. Si dice che un pezzo \"attacca\" o \"minaccia\" una casa
se esso può muoversi su di essa.\

### Alfiere

L'**alfiere** può muoversi su una qualunque casa della stessa diagonale
rispetto a quella in cui si trova, purché per raggiungerla non debba
attraversare case occupate da pezzi (amici o avversari) e purché la casa
d'arrivo non sia occupata da un pezzo amico. Ciascun alfiere non cambia
mai il colore delle case su cui si muove: per questo i giocatori parlano
di alfieri \"campochiaro\" o \"camposcuro\" a seconda del colore delle
case in cui si trovano.\

### Torre

La **torre** può muoversi su una qualunque casa della stessa traversa o
della stessa colonna rispetto a quella in cui si trova, purché per
raggiungerla non debba attraversare case occupate da pezzi (amici o
avversari) e purché la casa d'arrivo non sia occupata da un pezzo amico.
La torre è anche coinvolta nella speciale mossa del Re chiamata arrocco,
che sarà descritta più avanti.\

### Donna

La **donna** è il pezzo più potente di tutti e combina le mosse
dell'alfiere con quelle della torre, potendo quindi muoversi su tutte le
case della stessa traversa, della stessa colonna o della stessa
diagonale della casa su cui si trova. A differenza della torre,
tuttavia, non può prendere parte all'arrocco.\

### Cavallo

Il **cavallo** può muoversi su una delle case a lui più vicine che non
appartengono alla traversa, alla colonna e alle diagonali passanti per
la sua casa di partenza. Un cavallo al centro della scacchiera ha a
disposizione otto case (\"rosa di cavallo\") verso le quali muoversi,
mentre se si trova al bordo la sua mobilità è ridotta a quattro case e
se si trova in un angolo a due. Il movimento del cavallo può essere
immaginato come la somma di uno spostamento orizzontale di una casa e di
uno verticale di due o viceversa, descrivendo una \"L\". Il cavallo è
inoltre l'unico pezzo che, nei suoi movimenti, può attraversare anche
caselle già occupate da altri pezzi: si dice che può \"saltare\". Si
noti che a ogni mossa il cavallo cambia il colore della casa in cui si
trova.\

### pedone

Il **Pedone** segue regole di movimento leggermente più complesse:\
Alla sua prima mossa, ossia quando parte dalla posizione iniziale,
ciascun pedone può muovere di una oppure due case in avanti, a scelta
del giocatore, a patto che la casa di destinazione ed eventualmente la
casa saltata siano libere. Nelle sue mosse successive il pedone può
avanzare solo di una casa per mossa, a patto che questa sia libera. A
differenza degli altri pezzi, non può muovere all'indietro.\
Il pedone è l'unico pezzo che cattura in maniera differente da come
muove. Può catturare un pezzo nemico solo se si trova su una delle due
case poste diagonalmente in avanti rispetto alla sua casa di partenza
(vedi diagramma), ma non può né muovere in tali case se esse sono libere
né catturare i pezzi che si trovano nelle case che ha di fronte. Quando,
eseguendo la sua prima mossa di due case in avanti, il pedone viene a
trovarsi di fianco a un pedone avversario, quest'ultimo può alla mossa
successiva catturarlo *en passant*, come se il primo fosse avanzato di
una sola casa. La presa *en passant* può essere eseguita solo come mossa
successiva all'avanzamento del pedone avversario di due case.\
Se un pedone riesce ad avanzare fino all'ottava traversa, viene
promosso, ossia assume il ruolo e le capacità di movimento di un altro
pezzo dello stesso colore (donna, torre, alfiere o cavallo) a scelta del
giocatore, indipendentemente dai pezzi già presenti sulla scacchiera. In
questo modo è dunque possibile avere un numero di esemplari di un certo
pezzo maggiore rispetto a quello iniziale. L'effetto è immediato, ad
esempio si può dare scacco o scacco matto con una promozione se il re
avversario è nel raggio di azione del nuovo pezzo. Nella pratica il
pedone viene quasi sempre promosso a donna, che è il pezzo più forte,
tuttavia in alcuni casi il massimo vantaggio possibile si ottiene
promuovendo il pedone ad un altro tipo di pezzo, ad esempio per dare
scacco col cavallo. In questi casi si parla di \"promozione minore\".\

### Re

Il **re** si può muovere in una delle case adiacenti (anche
diagonalmente) a quella occupata, purché questa non sia controllata da
un pezzo avversario. Una sola volta in tutta la partita ciascun re può
usufruire di una mossa speciale, nota come **arrocco**, che consiste nel
muovere il re di due case a destra o a sinistra in direzione di una
delle due torri e successivamente (ma sempre durante lo stesso turno)
muovere la torre (quella verso la quale il re si è mosso) nella casa
compresa tra quelle di partenza e di arrivo del re. Questo si può fare
solamente se tutte le condizioni seguenti sono soddisfatte:

-   Il giocatore non ha ancora mosso né il re né la torre coinvolta
    nell'arrocco;

-   Non ci devono essere pezzi (amici o avversari) fra il re e la torre
    utilizzata;

-   Né la casa di partenza del re, né la casa che esso deve
    attraversare, né quella di arrivo devono essere minacciate da un
    pezzo avversario, cioè il re non deve trovarsi sotto scacco né prima
    né dopo né l'arrocco.

Il re è l'unico pezzo che non viene mai catturato, ma solo minacciato.
Quando il re di uno dei due giocatori è minacciato, trovandosi sulla
traiettoria di un pezzo nemico, si dice che è e non è consentita alcuna
mossa che lasci il proprio re in tale condizione. Deve quindi essere
effettuata una mossa che elimini la minaccia in uno dei seguenti modi:

-   Muovere il re in una delle case adiacenti, a patto che questa non
    sia sotto il controllo di un altro pezzo avversario;

-   Catturare, con il re o con un altro pezzo, il pezzo avversario che
    si trova sulla traiettoria del re e dà origine allo scacco;

-   Nel caso di minaccia da parte di donna, torre o alfiere non
    adiacenti al re sotto attacco, frapporre tra quest'ultimo e il pezzo
    che minaccia scacco un qualunque pezzo o pedone, in modo che sia
    quest'ultimo a essere minacciato invece del re.

Se nessuna delle mosse che il giocatore può effettuare è in grado di
liberare il re dallo scacco, si tratta di **scacco matto** e la partita
termina con la vittoria dell'avversario. Se invece il re non si trova
sotto scacco ma non è possibile effettuare alcuna mossa legale (ad
esempio se si ha solo il re in gioco e questo non è sotto scacco, ma
tutte le case libere a esso adiacenti sono minacciate), si tratta di
stallo e la partita termina con un risultato di parità, non potendo il
giocatore che si trova in questa condizione muovere senza contravvenire
a qualche regola del gioco.\

## Conclusione della partita

Lo scopo degli scacchi consiste nel dare **scacco matto** al re
avversario. Si ha scacco matto quando il re si trova sotto la minaccia
diretta dei pezzi avversari e non ha possibilità di sottrarsi a essa,
quindi sarebbe sicuramente catturato alla mossa successiva se non si
trattasse del re. Lo scacco matto determina la conclusione della partita
con la sconfitta del giocatore che lo subisce. Lo **scacco** invece è
l'attacco che un pezzo avversario porta al re e da cui il re può essere
protetto. Non è necessario che chi fa una mossa che pone il re
avversario sotto scacco lo annunci verbalmente; al contrario, nelle
partite ufficiali di torneo, tale comportamento può essere ritenuto
fastidioso e sanzionabile con una ammonizione o un richiamo. Il
regolamento non consente di eseguire alcuna mossa che metta o lasci il
proprio re sotto . La partita può terminare anche per abbandono da parte
di un contendente, ovviamente con la vittoria dell'altro.\
Il gioco termina in **Patta** (parità) nei seguenti casi:

-   Se restano sulla scacchiera soltanto i due re;

-   Se la situazione è tale per cui nessuno dei due giocatori può dare
    scacco matto all'altro, anche in caso di difesa peggiore. Ad esempio
    re e cavallo o re e alfiere contro re è patta, ma non re e due
    cavalli contro re oppure re e cavallo contro re e cavallo, dato che
    una posizione di matto esiste (anche se a gioco corretto, ovvero in
    assenza di errori dell'avversario, non può essere forzata);

-   Se il giocatore che ha il tratto non può compiere alcuna mossa
    legale ma il suo re non è sotto scacco (stallo).

La partita termina con una patta su richiesta di un giocatore se questi
effettua o ha intenzione di effettuare (avendola segnata sul formulario
e avendo espresso l'intenzione di eseguirla) una mossa che:

-   Sarà l'ultima di una serie di cinquanta mosse consecutive (cinquanta
    mosse per ciascun giocatore) nelle quali non è stato catturato alcun
    pezzo e non è stato mosso alcun pedone;

-   Comporta la ripetizione sulla scacchiera della stessa posizione per
    tre volte (anche non consecutive) durante la partita. La posizione è
    considerata identica se la mossa spetta al medesimo giocatore, se
    tutti i pezzi del medesimo genere e colore si trovano sulle stesse
    case e se sussistono inalterate le stesse possibilità di movimento
    (inclusi arrocco e catture en passant).

Nella pratica di torneo il giocatore si rivolgerà all'arbitro il quale,
dopo aver verificato che le condizioni necessarie siano soddisfatte, ha
l'obbligo di dichiarare patta la partita.\
Inoltre, in qualsiasi momento della partita, salvo speciali limitazioni
imposte in singoli tornei, uno dei due giocatori può proporre la patta
all'avversario, che ha naturalmente il diritto di rifiutarla. Se la
accetta, la partita termina immediatamente con il pareggio.\

## Gioco a tempo

Nella maggior parte delle partite di club e fra professionisti il gioco
degli scacchi si disputa a tempo, usando un orologio doppio come quello
in figura [5.1](#fig:time){reference-type="ref" reference="fig:time"},
munito di due pulsanti: ognuno dei due giocatori, eseguita la sua mossa,
aziona l'orologio, arrestando il proprio e mettendo in moto quello
dell'avversario. L'orologio è munito di una piccola lancetta
supplementare, detta in gergo \"bandierina\", che quando rimangono
cinque minuti comincia a sollevarsi lentamente, abbassandosi poi di
colpo quando il tempo è scaduto. La bandierina deve essere chiaramente
visibile e non deve essere nascosta da eventuali riflessi dell'orologio.
Lo schermo di un orologio deve essere leggibile da almeno 3 metri e il
fatto che un orologio sia in funzione deve essere comprensibile da
almeno 10 metri di distanza. L'orologio deve funzionare in maniera più
silenziosa possibile, per non disturbare i giocatori.

![image](images/time_chess.png){#fig:time width="\\textwidth"}
[\[fig:time\]]{#fig:time label="fig:time"}

Le caratteristiche e l'uso dell'orologio sono regolamentate
ufficialmente.\
La quantità di tempo inizialmente a disposizione dei giocatori (la
cadenza di gioco) può variare.\
Le partite si distinguono in:

-   Tempo lungo: partite con più di 60 minuti a testa. Hanno una durata
    che può superare le 7 ore di gioco: in molti tornei infatti si
    utilizza una cadenza di 2 ore a testa per le prime 40 mosse, con una
    prima aggiunta di 60 minuti al termine della 40-esima mossa e una
    seconda aggiunta di 30 minuti al termine della 60-esima mossa,
    proseguendo il gioco con un'ulteriore piccola aggiunta di tempo per
    ogni mossa.

-   Gioco rapido (*Rapidplay*): la cadenza di gioco prevede un tempo di
    riflessione variabile tra i 15 e i 60 minuti.

-   Lampo (*Blitz*):la cadenza di gioco prevede un tempo di riflessione
    minore di 15 minuti.

Il classico orologio per scacchi è meccanico, ma è ormai diffusissima, e
di fatto irrinunciabile nei tornei internazionali, la sua versione
elettronica, che è in grado di eseguire automaticamente alcune
operazioni, quali memorizzare il numero di mosse giocate, variare la
cadenza di gioco nel corso della partita, aggiungere una determinata
quantità di tempo a ogni giocatore per ogni mossa giocata (se previsto
dal regolamento del torneo). Gli orologi digitali per essere
regolamentari non devono consentire il cambiamento della data in maniera
immediata e devono fornire una indicazione se la carica della batteria è
in esaurimento (garantendo comunque almeno 10 ore di autonomia in tale
condizione).\

## Notazione delle mosse

Il sistema utilizzato comunemente per annotare le mosse, chiamato
notazione algebrica, è descritto nel regolamento internazionale degli
scacchi. Nei tornei ufficiali infatti ciascun giocatore deve annotare le
mosse su un verbale detto formulario; fa eccezione il gioco rapido, per
il quale, vista la cadenza di gioco breve, i giocatori sono esentati
dalla verbalizzazione delle mosse.\
La notazione algebrica è l'unico sistema ufficialmente riconosciuto
dalla FIDE e fa uso di lettere, numeri e simboli per l'annotazione delle
mosse ed eventuali commenti alle stesse.\
Le mosse vengono numerate in ordine progressivo e sono composte ciascuna
da una coppia di semimosse. La semimossa riporta una lettera maiuscola a
indicare il tipo di pezzo mosso (tranne per il pedone, per il quale non
è prevista alcuna lettera): l'alfabeto delle lettere varia da lingua a
lingua. Segue la casa di destinazione (individuata da una coppia di
coordinate: lettera per la colonna, numero per la traversa) preceduta da
una \"x\" nel caso di cattura; nelle catture effettuate dai pedoni si
indica prima della \"x\", con lettera minuscola, la colonna di
provenienza del pedone, e nel caso di cattura *en passant* la semimossa
di cattura è seguita dall'indicazione \"e.p.\".\
La promozione si indica ponendo al termine della mossa il simbolo \"=\"
seguito dalla lettera indicante il pezzo richiesto per la promozione
stessa.\
L'arrocco ha un simbolo particolare: \"0-0-0\" (lungo) e \"0-0\"
(corto).\
Un \"+\" al termine della semimossa indica uno scacco (a volte si usa
\"++\" per indicare uno scacco doppio), mentre un \"\#\" indica lo
scacco matto. Esiste poi un ampio ventaglio di simboli impiegati dai
commentatori: i più comuni sono \"!\" e \"!!\" (buona/ottima mossa),
\"?\" e \"??\" (errore/errore grave), \"!?\" e \"?!\" (mossa
interessante/dubbia).\
Ecco un semplice esempio di notazione algebrica:\
\

[^1]: \* Non trattato in questo documento
